#!/bin/bash

PWD=`pwd`

mkdir -p ~/.terminfo
tic -o ~/.terminfo/ ./terminfo/tmux.terminfo
tic -o ~/.terminfo/ ./terminfo/tmux-256color.terminfo
tic -o ~/.terminfo/ ./terminfo/xterm-256color.terminfo

ln -s ${PWD}/vimrc ~/.vimrc
vim +PlugInstall +qall
ln -s ${PWD}/indent ~/.vim/indent

