set nocompatible
" the above setting is needed for both vim-plug and vimwiki

" Auto installs vim-plug if it is not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" Automatically install missing plugins on startup
if !empty(filter(copy(g:plugs), '!isdirectory(v:val.dir)'))
  autocmd VimEnter * PlugInstall | q
endif

" My plugins

" Git wrapper for vim. run git commands form withing vim
Plug 'tpope/vim-fugitive'

" The airline status bar gives you file status etc.
Plug 'bling/vim-airline'

" Most handy systax checking plugin. Uset it for editing python files
" Additing synstastic done below.
Plug 'vim-scripts/Syntastic'

" Very handy to comment a large block of text in vim
Plug 'tpope/vim-commentary'
" Should consider replacing this with the below plugin
" https://github.com/tomtom/tcomment_vim
" one day

" Sensible defaults for vim
Plug 'tpope/vim-sensible'

" Plugin to display training whitespace chars and tabs
Plug 'ntpeters/vim-better-whitespace'

" Plugin for code auto completion - jedi-vim
Plug 'davidhalter/jedi-vim'

" plug for ansible files
Plug 'pearofducks/ansible-vim'

" plug for indent guides
Plug 'nathanaelkane/vim-indent-guides'

" Color Schemes
Plug 'vim-scripts/Mustang2'
Plug 'vim-scripts/molokai'
Plug 'vim-scripts/tir_black'
Plug 'vim-scripts/peaksea'
Plug 'vim-scripts/fruity.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'morhetz/gruvbox'

" vimwiki plug for note taking
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }

" plugin to view calendar in vim
Plug 'mattn/calendar-vim'

call plug#end()

filetype plugin on
syntax enable
set background=dark
set cuc

" enable 256 colors for vim
set t_Co=256
" Now going to attemp to enable truecolors for vim
" the below setting enabled truecolors. depends on your version of vim
" Hence why we prefixing it in silent to allow for silent failure
silent! set termguicolors

" Setting color scheme
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_italic=1  " enable italic fonts for gruvbox
silent! colorscheme gruvbox

set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4

" configuration to enable vim-airline status bar
set laststatus=2

" configure syntastic python checker to use flake8
let g:syntastic_python_checkers=['flake8']


" Code folding settings
" http://smartic.us/2009/04/06/code-folding-in-vim/
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" Enabling spell check - still a WIP and will need to be tweaked
set spell spelllang=en_ca
" Ref: https://www.linux.com/training-tutorials/using-spell-checking-vim/
" To move to a misspelled word, use "]s" and "[s". The "]s" command will move
" the cursor to the next misspelled word, the "[s" command will move the
" cursor back through the buffer to previous misspelled words.
" Once the cursor is on the word, use z=, and Vim will suggest a list of
" alternatives that it thinks may be correct.
" What if Vim is wrong, and the word is correct? Use the "zg" command and Vim
" will add it to its dictionary. Simple as pie. You can also mark words as
" incorrect using "zw".

" will remove any training white spaces before a Buffer Write.
autocmd BufWritePre * %s/\s\+$//e

" Delete trailing white spaces in vim.
" http://vimcasts.org/episodes/tidying-whitespace/
" http://vimcasts.org/episodes/tidying-whitespace/
function! Preserve(command)
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  execute a:command
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction
nmap _$ :call Preserve("%s/\\s\\+$//e")<CR>

" Enable paste mode on by default
" set paste
" Disabling as it was messing with autoindent
" when writing python scripts

" Set column width to 80 columns
" Been writing way too much python scripts
set colorcolumn=80

" Set line highlight for the cursor line
set cursorline

" set my backspace as it stopped working normally in vim
" Commenting this out in favour of vim-sensible plugin
" set backspace=indent,eol,start

" vimwiki setting to use markdown syntax
let g:vimwiki_list = [{'path': '~/notes/', 'syntax': 'markdown', 'ext': '.md'}]

" Adding these to allow me to autocommit my notes to git
set exrc " enables per-directory .vimrc files
set secure " disables unsafe commands in local .vimrc files
